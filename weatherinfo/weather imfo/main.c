#include <stdio.h>
#include <stdlib.h>

struct weather
    {
        int temp;
        int humi;
    };
struct weather a[30];
void SetWeather();
void GetWeather();

int main()
{
     SetWeather();
     GetWeather();

    return 0;
}

void SetWeather()
{
    int i;
    int day;
    printf("Set Weather info :\n");
    printf("Day :");
    scanf("%d",&day);
    i=day-1;
    printf("Temp :");
    scanf("%d",&a[i].temp);
    printf("Humidity :");
    scanf("%d",&a[i].humi);
    printf("\nDo you want to set anther temp info [Y:N]: ");
    char ans='n';
    scanf("%s",&ans);
    if (ans=='y' || ans=='Y')
        SetWeather();

}
void GetWeather()
{
    int y;
    int x;
    printf("Enter the day you want to know its weather :");
    scanf("%d",&x);
    y=x-1;
    printf("Temp : %d \n",a[y].temp);
    printf("Humidity : %d \n",a[y].humi);
    printf("Do you want to get anther temp info [Y:N]: ");
    char ans='n';
    scanf("%s",&ans);
    if (ans=='y' || ans=='Y')
        GetWeather();

}
